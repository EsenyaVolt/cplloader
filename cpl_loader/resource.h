﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется cplloader.rc
//
#define IDD_CPL_LOADER_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_OBZOR                       1001
#define IDC_EDIT2                       1002
#define IDC_EDIT3                       1003
#define IDC_PROCESS                     1004
#define IDC_CLOSE                       1005
#define IDC_ZNACH                       1006
#define IDC_EDIT4                       1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
