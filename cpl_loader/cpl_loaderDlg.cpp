﻿
// cpl_loaderDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "cpl_loader.h"
#include "cpl_loaderDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CcplloaderDlg



CcplloaderDlg::CcplloaderDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CPL_LOADER_DIALOG, pParent)
	, Adress(_T(""))
	, Name(_T(""))
	, Opisanie(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CcplloaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Adress);
	DDX_Text(pDX, IDC_EDIT2, Name);
	DDX_Text(pDX, IDC_EDIT3, Opisanie);
	DDX_Control(pDX, IDC_ZNACH, Picture);
	DDX_Control(pDX, IDC_PROCESS, DO);
}

BEGIN_MESSAGE_MAP(CcplloaderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OBZOR, &CcplloaderDlg::OnBnClickedObzor)
	ON_BN_CLICKED(IDC_PROCESS, &CcplloaderDlg::OnBnClickedProcess)
	ON_BN_CLICKED(IDC_CLOSE, &CcplloaderDlg::OnBnClickedClose)
END_MESSAGE_MAP()


// Обработчики сообщений CcplloaderDlg

BOOL CcplloaderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CcplloaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CcplloaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CcplloaderDlg::OnBnClickedObzor()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	CFileDialog OpenF(TRUE, L"cpl", NULL, OFN_HIDEREADONLY, L" (*.cpl |*.cpl||)");

	if (OpenF.DoModal() == IDOK)
	{
		Name = OpenF.GetPathName();
		Adress = Name;

		if (loadlib != NULL)
		{
			if (!FreeLibrary(loadlib))
			{
				MessageBox((CString)"Ошибка выгрузки cpl", NULL, MB_OK);
				return;
			}
		}

		loadlib = LoadLibrary(Name);

		if (loadlib == NULL)
		{
			MessageBox((CString)"Ошибка загрузки cpl", NULL, MB_OK);
			return;
		}

		ProcAdr = (APPLET_PROC)GetProcAddress(loadlib, "CPlApplet");

		if (ProcAdr == NULL)
		{
			MessageBox((CString)"Ошибка", NULL, MB_OK);
			return;
		}

		if (!ProcAdr(NULL, CPL_INIT, 0, 0))
		{
			MessageBox((CString)"Ошибка", NULL, MB_OK);
			return;
		}

		if (ProcAdr(NULL, CPL_GETCOUNT, 0, 0) > 0)
		{
			CPLINFO inf;
			ProcAdr(NULL, CPL_INQUIRE, 0, (LPARAM)&inf);

			HICON icon = LoadIcon(loadlib, MAKEINTRESOURCE(inf.idIcon));
			Picture.SetIcon(icon);

			Name.LoadString(loadlib, inf.idName);
			Opisanie.LoadString(loadlib, inf.idInfo);
			DO.EnableWindow(TRUE);
		}
	}

	UpdateData(FALSE);
}

void CcplloaderDlg::OnBnClickedProcess()
{
	// TODO: добавьте свой код обработчика уведомлений
	ProcAdr(m_hWnd, CPL_DBLCLK, 0, 0);
}

void CcplloaderDlg::OnBnClickedClose()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}
