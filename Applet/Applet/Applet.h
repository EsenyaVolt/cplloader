﻿// Applet.h: основной файл заголовка для библиотеки DLL Applet
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


class CAppletApp : public CWinApp
{
public:
	CAppletApp();


public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};