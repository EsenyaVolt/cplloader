﻿// Interface.cpp: файл реализации
//

#include "pch.h"
#include "Interface.h"
#include "afxdialogex.h"
#include "Applet.h"
#include "stdafx.h"
#include "windows.h"
#include "mmsystem.h"

// Диалоговое окно Interface

IMPLEMENT_DYNAMIC(Interface, CDialog)

Interface::Interface(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_INTERFACE, pParent)
{

}

Interface::~Interface()
{
}

void Interface::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Interface, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &Interface::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Interface::OnBnClickedButton2)
END_MESSAGE_MAP()

// Обработчики сообщений Interface


void Interface::OnBnClickedButton1()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	ShellExecute(NULL, L"open", L"cmd.exe", L"/?", NULL, SW_SHOWNORMAL);

	UpdateData(FALSE);
}


void Interface::OnBnClickedButton2()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	system("write");

	UpdateData(FALSE);
}
