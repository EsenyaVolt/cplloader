﻿// Applet.cpp: определяет процедуры инициализации для библиотеки DLL.
//

#include "pch.h"
#include "framework.h"
#include "Applet.h"
#include "stdafx.h"
#include <Cpl.h>
#include "Interface.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAppletApp

BEGIN_MESSAGE_MAP(CAppletApp, CWinApp)
END_MESSAGE_MAP()

// Создание CAppletApp

CAppletApp::CAppletApp()
{
	// TODO: добавьте код создания,
	// Размещает весь важный код инициализации в InitInstance
}

// Единственный объект CAppletApp

CAppletApp theApp;

// Инициализация CAppletApp

BOOL CAppletApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

LONG WINAPI CPlApplet(HWND hwnd, UINT msg, LPARAM par1, LPARAM par2)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	switch (msg)
	{
		case CPL_INIT: return TRUE;
		case CPL_GETCOUNT: return 1;
		case CPL_INQUIRE:
		{
			CPLINFO* str = (CPLINFO*)par2;
			str->idIcon = IDI_ICON1;
			str->idInfo = IDS_INFO;
			str->idName = IDS_NAME;

			str->idInfo = IDS_INFO1;
			str->idName = IDS_NAME1;
			break;
		};
		case CPL_DBLCLK:
		{
			Interface Dlg;
			Dlg.DoModal();
			break;
		}
	}

	return 0;
}