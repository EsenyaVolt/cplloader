﻿#pragma once


// Диалоговое окно Interface

class Interface : public CDialog
{
	DECLARE_DYNAMIC(Interface)

	public:
		Interface(CWnd* pParent = NULL);   // стандартный конструктор
		virtual ~Interface();

		// Данные диалогового окна
	#ifdef AFX_DESIGN_TIME
		enum { IDD = IDD_INTERFACE };
	#endif

	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

		DECLARE_MESSAGE_MAP()
	public:
		afx_msg void OnBnClickedButton1();
		afx_msg void OnBnClickedButton2();
};